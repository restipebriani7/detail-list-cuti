/**
 * 
 */
package com.myindo.project.inquirydetaillistpengajuancuti.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myindo.project.inquirydetaillistpengajuancuti.connection.Connect;
import com.myindo.project.inquirydetaillistpengajuancuti.model.HasilPengajuanCuti;
import com.myindo.project.inquirydetaillistpengajuancuti.model.RequestModel;

/**
 * @author Resti Pebriani
 *
 */
public class GetDetailListPCuti {
	Logger log = Logger.getLogger("GetDetailListPCuti");
	String hasilGet;
	HasilPengajuanCuti hCuti;

	public String inquiryDetailCuti(RequestModel rModel) throws Exception {

		hCuti = new HasilPengajuanCuti();
		Connection con = Connect.connection();
		try {
			PreparedStatement ps = con.prepareStatement("SELECT * FROM pengajuan_cuti WHERE npk=? AND id_pengajuan=?");
			ps.setString(1, rModel.getNpk());
			ps.setString(2, rModel.getId_pengajuan());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				hCuti.setId_pengajuan(rs.getString("id_pengajuan"));
				hCuti.setTgl_cuti(rs.getString("tgl_cuti"));
				hCuti.setJml_hari(rs.getString("jml_hari"));
				hCuti.setKet_cuti(rs.getString("ket_cuti"));
				hCuti.setTgl_masuk(rs.getString("tgl_masuk"));
				hCuti.setNpk(rs.getString("npk"));
			}
			rs.close();
			System.out.println("Npk : " + rModel.getNpk());

			if (!hCuti.getId_pengajuan().trim().isEmpty() && hCuti.getId_pengajuan() != null) {
				ObjectMapper mapper = new ObjectMapper();
				hasilGet = mapper.writeValueAsString(hCuti);
				System.out.println("Hasil Get Pengajuan Cuti: " + hasilGet);

				log.info("Select data pengajuan cuti berdasarkan id_pengajuan dan npk berhasil");
				con.close();
			} 
		} catch (Exception e) {
			// TODO: handle exception
			log.info("Select data gagal");
			log.info(e.getMessage());
			hCuti = new HasilPengajuanCuti();
			
			hCuti.setId_pengajuan("");
			hCuti.setTgl_cuti("");
			hCuti.setJml_hari("");
			hCuti.setKet_cuti("");
			hCuti.setTgl_masuk("");
			hCuti.setNpk("");
			
			ObjectMapper mapper = new ObjectMapper();
			hasilGet = mapper.writeValueAsString(hCuti);
			System.out.println("Hasil Get catch : " + hasilGet);
		}
		return hasilGet;
	}
}
