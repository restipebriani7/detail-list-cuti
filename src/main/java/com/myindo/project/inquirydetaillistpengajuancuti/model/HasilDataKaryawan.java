/**
 * 
 */
package com.myindo.project.inquirydetaillistpengajuancuti.model;

/**
 * @author Resti Pebriani
 *
 */
public class HasilDataKaryawan {
	private String name;
	private String position;
	private String department;
	private String npk;
	private String sisa_cuti;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	public String getSisa_cuti() {
		return sisa_cuti;
	}
	public void setSisa_cuti(String sisa_cuti) {
		this.sisa_cuti = sisa_cuti;
	}
	@Override
	public String toString() {
		return "HasilDataKaryawan [name=" + name + ", position=" + position + ", department=" + department + ", npk="
				+ npk + ", sisa_cuti=" + sisa_cuti + "]";
	}
}
