/**
 * 
 */
package com.myindo.project.inquirydetaillistpengajuancuti.model;

/**
 * @author Resti Pebriani
 *
 */
public class HasilPengajuanCuti {
	private String id_pengajuan;
	private String tgl_cuti;
	private String jml_hari;
	private String ket_cuti;
	private String tgl_masuk;
	private String npk;
	
	public String getId_pengajuan() {
		return id_pengajuan;
	}
	public void setId_pengajuan(String id_pengajuan) {
		this.id_pengajuan = id_pengajuan;
	}
	public String getTgl_cuti() {
		return tgl_cuti;
	}
	public void setTgl_cuti(String tgl_cuti) {
		this.tgl_cuti = tgl_cuti;
	}
	public String getJml_hari() {
		return jml_hari;
	}
	public void setJml_hari(String jml_hari) {
		this.jml_hari = jml_hari;
	}
	public String getKet_cuti() {
		return ket_cuti;
	}
	public void setKet_cuti(String ket_cuti) {
		this.ket_cuti = ket_cuti;
	}
	public String getTgl_masuk() {
		return tgl_masuk;
	}
	public void setTgl_masuk(String tgl_masuk) {
		this.tgl_masuk = tgl_masuk;
	}
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	@Override
	public String toString() {
		return "HasilPengajuanCuti [id_pengajuan=" + id_pengajuan + ", tgl_cuti=" + tgl_cuti + ", jml_hari=" + jml_hari
				+ ", ket_cuti=" + ket_cuti + ", tgl_masuk=" + tgl_masuk + ", npk=" + npk + "]";
	}
	
}
